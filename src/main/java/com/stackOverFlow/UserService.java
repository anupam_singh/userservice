package com.stackOverFlow;

import com.stackOverFlow.common.RestAPIVerticle;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.ReplyException;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.FormLoginHandler;
import io.vertx.ext.web.handler.JWTAuthHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.sstore.LocalSessionStore;

public class UserService extends RestAPIVerticle {

//	public static final String MONGODB = "localhost";
	
//	public static final String MONGODB = "mongodbhost";
	
	public static final String MONGODB = "104.197.175.52";
	
	public static final String MONGO_PORT = ":27017";
//	public static final String MONGO_PORT = ":8080";
	
	private static final int DEFAULT_PORT = 8050;
	
	private static final String SERVICE_NAME = "userservice";
	
	@Override
	public void start(Future<Void> startFuture) throws Exception {
		String host = config().getString("http.address", "localhost");
		int port = config().getInteger("http.port", DEFAULT_PORT);
		System.out.println("UserService.start() host: "+host);
		Router router = Router.router(vertx);
		
		// body handler
		router.route().handler(CookieHandler.create());
		router.route().handler(BodyHandler.create());
		router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)));
		vertx.deployVerticle("com.stackOverFlow.UserVerticle", new DeploymentOptions().setWorker(true).setInstances(2));
		
		JsonObject config = new JsonObject();
		config.put("db_name", "stackoverflow");
//		config.put("connection_string", "mongodb://" + MONGODB + ":27017");
		config.put("connection_string", "mongodb://" + MONGODB + MONGO_PORT);
		MongoClient client = MongoClient.createShared(vertx, config);
	    JsonObject authProperties = new JsonObject();
	    authProperties.put(MongoAuth.PROPERTY_COLLECTION_NAME, "users");
		authProperties.put(MongoAuth.PROPERTY_USERNAME_FIELD, "_id");
//		MongoAuth authProvider = MongoAuth.create(client, authProperties);
		MongoAuth authProvider = new MyAuthProvider(client, authProperties);
		
		// Create a JWT Auth Provider
	    JWTAuth jwt = JWTAuth.create(vertx, new JsonObject()
	        .put("keyStore", new JsonObject()
	            .put("type", "jceks")
	            .put("path", "keystore.jceks")
	            .put("password", "secret")));
		
		Handler<RoutingContext> handler = JWTAuthHandler.create(jwt);
		router.route(HttpMethod.GET,"/users/").handler(handler);
		router.route(HttpMethod.GET,"/users/logout/").handler(handler);
		System.out.println("UserService.start() authProvider: "+authProvider);
		router.route("/users/login").handler(new CustomFormLoginHandler(authProvider, FormLoginHandler.DEFAULT_USERNAME_PARAM, FormLoginHandler.DEFAULT_PASSWORD_PARAM, FormLoginHandler.DEFAULT_RETURN_URL_PARAM, null,jwt));
		
		router.get("/users/").handler(rctx -> {
//	    	System.out.println("WebserverVerticle.start() users rctx: "+rctx);
//	    	System.out.println("WebserverVerticle.start() GET USERS rctx.getBodyAsJson(): "+rctx.getBodyAsJson());
//	    	System.out.println("WebserverVerticle.start() GET USERS rctx.getBodyAsJson().encodePrettily(): "+rctx.getBodyAsJson().encodePrettily());
	    	System.out.println("UserService.start() GET USERS rctx.user(): "+rctx.user());
	    	if(rctx.user() == null) {
				rctx.response().setStatusCode(401).putHeader("Content-Type", "application/json").end("Not Authorized");
	    		return;
	    	}
	    	System.out.println("UserService.start() rctx.user().principal(): "+rctx.user().principal());
			vertx.eventBus().send("read.users", rctx.user().principal(), res -> {
				if(res.succeeded()) {
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
					.end(res.result().body().toString());
				} else if(res.failed()) {
					rctx.response().setStatusCode(((ReplyException)res.cause()).failureCode()).putHeader("Content-Type", "application/json")
					.end(res.cause().getLocalizedMessage());
				}
			});
		});
		
		router.post("/users/").handler(rctx -> {
	    	System.out.println("UserService.start() POST USERS rctx.getBodyAsJson().encodePrettily(): "+rctx.getBodyAsJson().encodePrettily());
			vertx.eventBus().send("write.users", rctx.getBodyAsJson().encodePrettily(), res -> {
				if(res.succeeded()) {
					rctx.response().setStatusCode(200).putHeader("Content-Type", "application/json")
					.end(res.result().body().toString());
				} else if(res.failed()) {
					rctx.response().setStatusCode(((ReplyException)res.cause()).failureCode()).putHeader("Content-Type", "application/json")
					.end(res.cause().getLocalizedMessage());
				}
			});
		});
		
		router.get("/users/logout/").handler(rctx -> {
			System.out.println("UserService.start() Authorization header: "+rctx.request().getHeader("Authorization"));
			rctx.clearUser();
			rctx.response().setStatusCode(200).putHeader("Content-Type", "text/html").end();
		});
		
		
		router.route(HttpMethod.POST,"/questions/").handler(handler);
		router.route(HttpMethod.PATCH,"/questions/").handler(handler);
		
		router.route(HttpMethod.POST,"/questions/").handler(rctx -> {
			System.out.println("WebserverVerticle.start() POST USERS rctx.user(): "+rctx.user());
	    	if(rctx.user() == null) {
				rctx.response().setStatusCode(401).putHeader("Content-Type", "application/json").end("Not Authorized");
	    		return;
	    	}
	    	JsonObject usernameObj = new JsonObject();
	    	usernameObj.put("username", rctx.user().principal().getString("username"));
	    	System.out.println("WebserverVerticle.start() rctx.user().principal(): "+rctx.user().principal());
	    	System.out.println("UserService.start() usernameObj.encodePrettily(): "+usernameObj.encodePrettily());
			rctx.response().setStatusCode(404).putHeader("Content-Type", "application/json")
					.end(usernameObj.encodePrettily());
		});
		
		router.route(HttpMethod.PATCH,"/questions/").handler(rctx -> {
			System.out.println("WebserverVerticle.start() PATCH USERS rctx.user(): "+rctx.user());
	    	if(rctx.user() == null) {
				rctx.response().setStatusCode(401).putHeader("Content-Type", "application/json").end("Not Authorized");
	    		return;
	    	}
	    	JsonObject usernameObj = new JsonObject();
	    	usernameObj.put("username", rctx.user().principal().getString("username"));
	    	System.out.println("WebserverVerticle.start() rctx.user().principal(): "+rctx.user().principal());
	    	System.out.println("UserService.start() usernameObj.encodePrettily(): "+usernameObj.encodePrettily());
			rctx.response().setStatusCode(404).putHeader("Content-Type", "application/json")
					.end(usernameObj.encodePrettily());
		});
		
		vertx.createHttpServer().requestHandler(router::accept).listen(DEFAULT_PORT);
		// create HTTP server and publish REST service
//		createHttpServer(router, host, port).compose(serverCreated -> publishHttpEndpoint(SERVICE_NAME, host, port,"/users/"));
//		createHttpServer(router, host, port);
		
		
	}

}
