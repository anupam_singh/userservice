package com.stackOverFlow;

import io.vertx.core.Future;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.sstore.LocalSessionStore;

public class CloudUserService extends DefaultUserService {

	@Override
	protected void createSessionHandler(Router router) {
		router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)));
	}

	@Override
	protected String getMongoDBHost() {
		return "104.197.175.52";
	}

	@Override
	protected void createHttpServer(Router router, Future<Void> future) {
		vertx.createHttpServer().requestHandler(router::accept).listen(DEFAULT_PORT);
	}

}
